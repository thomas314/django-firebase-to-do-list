from django.shortcuts import render 
from django.shortcuts import redirect 
import pyrebase
# Importation du compte de service
# import firebase_admin
# from firebase_admin import credentials

# cred = credentials.Certificate("/home/revelt/Documents/iot/python/intro-django-firebase/djangofirebase/django-firebase-35dfd-firebase-adminsdk-kt1fd-ae037f7138.json")
# firebase_admin.initialize_app(cred)
# Configuration BDD
config = {
    'apiKey': "AIzaSyBjmE1N3pK1ueVLO2N0CwVRuouynwJzFd4",
    'authDomain': "django-firebase-35dfd.firebaseapp.com",
    'databaseURL': "https://django-firebase-35dfd.firebaseio.com",
    'projectId': "django-firebase-35dfd",
    'storageBucket': "django-firebase-35dfd.appspot.com",
    'messagingSenderId': "993303499867",
    'appId': "1:993303499867:web:da70e407bc4ce0c62e5b7e"
    # 'serviceAccount': 'firebase-adminsdk-kt1fd@django-firebase-35dfd.iam.gserviceaccount.com',
}

# Initialisation de l'application 
firebase = pyrebase.initialize_app(config)
auth = firebase.auth()

# Fonction signIn()
def singIn(request):
    return render(request, "signIn.html")

# Fonction postsign()
def postsign(request):
    email=request.POST.get('email')
    passw = request.POST.get("pass")

    try:
        user = auth.sign_in_with_email_and_password(email,passw)
    except:
        message = "invalid crediantials"  
        return render(request, "signIn.html", {"msg":message})

    # print(user)
    return render(request, "welcome.html",{"e":email})

db = firebase.database()

# Fonction de redirection vers la vue 'Créer une tâche'
def create(request):
    
    return render(request, "create.html")

# Fonction de redirection vers la vue 'Liste des tâches'
def liste(request):
    tasks=db.child("tasks").get()
    data = tasks
    listData = []
    for task in data.each():
        # o=task.val()
        key=task.key()
        nom=task.val() # Nom de la tâche
        resume=task.val() # Résumé de la tâche
        result= {
            "clé":key
            "nom":nom['name'],
            "resume":resume['resume'],
        }
        listData.append(result)
        
    print(listData)
    return render(request, "liste.html", {'data': listData})

# Fonction d'envoi de données, quand on créer une tâche
def post_create(request):
    
    name=request.POST.get('name')
    resume=request.POST.get('resume')

    data = {
        "name":name,
        "resume":resume,
    }
    task= db.child("tasks").push(data)
    # print(task)


    return redirect('/liste/')