"""djangofirebase URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin

from django.conf.urls import url 

from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.singIn),
    url(r'^postsign/', views.postsign),
    url(r'^create/', views.create, name='create'),
    url(r'^post_create/', views.post_create, name='post_create'),
    url(r'^liste/', views.liste, name='liste'),
]